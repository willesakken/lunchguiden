const express = require("express")


const app = express()

app.use("/api", require("./api/restaurants"))


app.listen(3000, () => console.log("App running on port 3000!"))

module.exports = app;