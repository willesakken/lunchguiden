let chai = require("chai")
let chaiHttp = require("chai-http")
let server = require("../server")
let expect = chai.expect

chai.use(chaiHttp)
describe("GET /api/restaurants", () => {
    it("should return all restaurants", done => {
        chai.request(server)
            .get("/api/restaurants")
            .then(res => {
                expect(res).to.have.status(200)
                expect(res.body).to.be.a("object")
                done()
            })
    })
})

describe("POST /api/restaurants", () => {
    it("should return 201 Created", done => {
        chai.request(server)
            .post("/api/restaurants")
            .then(res => {
                expect(res).to.have.status(201)
                done()
            })
    })
})