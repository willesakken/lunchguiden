const express = require('express')
const router = express.Router()

// Healthcheck endpoint
router.get("/restaurants", (req, res) => {
    res.status(200)
    res.send({name: "Apan", meals: ["Fläsk", "Kyckling", "Soppa"]})
})

router.post("/restaurants", (req, res) => {
    res.sendStatus(201)
})

module.exports = router