# Lunchguiden

Test project, simple rest api, builds, tests etc, depolys to aws... Hopefully.

## To run
### Get NVM
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
### Install node
nvm install node
nvm use node
nvm alias default node
### Run it
cd ./server/
npm start